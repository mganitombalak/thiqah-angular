var myVar="Muhammed";

function a() {
    var myVar="Gani";
    console.log(this);
    console.log("function a has been done!");
    b(myVar);
}

function b(name) {
    console.log("Hello " + name);
    console.log(this);
    console.log("function b has been done!");
}

a();
//b(myVar);