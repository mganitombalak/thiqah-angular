var obj = {
    name: "Gani",
    surname: "Tombalak",
    getFullName: () => console.log(this)
};//Literal JavaScript Object

console.log(obj.name);
console.log(obj.surname);
console.log(obj.getFullName);
console.log(obj.getFullName());
console.log("================================================")
function Person(name, surname) {
    this.name = name;
    this.surname = surname;
    this.getFullName = function () {
        console.log(this);
    };
}

var p = new Person("Muhammed", "Alown");
console.log(p.name);
console.log(p.surname);
console.log(p.getFullName);
console.log(p.getFullName());
p.getFullName();