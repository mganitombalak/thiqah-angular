interface IPerson {
    firstName: string;
    lastName: string;
    fullName(): void;
}
class Person implements IPerson{
    constructor(public firstName: string, public lastName: string) { }
    fullName() {
        this.firstName + " " + this.lastName;
    }
}

class Greeting {
    sayHello(p: IPerson) {
        console.log(`Hello ${p.fullName()}`);
    }
}
let person = new Person("Muhammed", "Alown");
let greeting = new Greeting();
greeting.sayHello({firstName:"Mehmet Gani",lastName:"Tombalak",fullName:()=>"hahaha"});