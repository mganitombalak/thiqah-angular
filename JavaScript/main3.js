function Person(name, surname) {
    this.name = name;
    this.surname = surname;
    this.getFullName = () => this.name + " " + this.surname;
}

var p = new Person("Muhammed", "Alown");
Person.prototype.doSomething = () => console.log("p object is doing something");
var p2 = new Person("Mehmet Gani", "Tombalak");

//console.log(p.getFullName);
console.log(p["name"]);
console.log(p2["name"]);