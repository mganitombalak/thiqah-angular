var Person = /** @class */ (function () {
    function Person(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    Person.prototype.fullName = function () {
        this.firstName + " " + this.lastName;
    };
    return Person;
}());
var Greeting = /** @class */ (function () {
    function Greeting() {
    }
    Greeting.prototype.sayHello = function (p) {
        console.log("Hello " + p.fullName());
    };
    return Greeting;
}());
var person = new Person("Muhammed", "Alown");
var greeting = new Greeting();
greeting.sayHello({ firstName: "Mehmet Gani", lastName: "Tombalak", fullName: function () { return "hahaha"; } });
