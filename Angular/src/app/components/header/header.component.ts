import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/modules/auth/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements AfterViewInit, OnDestroy {
  // private onAuthChangeSubs: Subscription;
  // buttonText = 'Sign In';
  constructor(private authService: AuthService) { }

  onSignOut() {
    this.authService.logout();
  }

  ngAfterViewInit(): void {
    // this.onAuthChangeSubs = this.authService.onAuthStatusChanged.subscribe(r => {
    //   if (r) {
    //     this.buttonText = 'Sign Out';
    //   } else {
    //     this.buttonText = 'Sign In';
    //   }
    // });
  }

  ngOnDestroy(): void {
    // this.onAuthChangeSubs.unsubscribe();
  }

}
