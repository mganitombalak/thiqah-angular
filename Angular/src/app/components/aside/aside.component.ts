import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/services/menu/menu.service';
import { IMenuItem } from 'src/app/common/models/IMenuItem';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.css']
})
export class AsideComponent implements OnInit {

  model: any; //Array<IMenuItem>;
  constructor(private menuService: MenuService) { }

  ngOnInit() {
    // this.menuService.findAll().subscribe(r => this.model = r.data);
    this.model = this.menuService.findAll();
  }

}
