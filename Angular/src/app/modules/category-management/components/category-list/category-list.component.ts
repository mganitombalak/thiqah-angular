import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { ICategory } from 'src/app/common/models/ICategory';
import { ActionGridCellComponent } from 'src/app/modules/shared/components/action-grid-cell/action-grid-cell.component';
import { ModalService } from 'src/app/modules/shared/services/modal.service';
import { CategoryDetailsComponent } from '../category-details/category-details.component';
import { AgGridEvent } from 'ag-grid-community';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {
  constructor(private categoryService: CategoryService, private modalService: ModalService) { }

  columnDefs = [
    { headerName: 'Display Order', field: 'displayOrder' },
    { headerName: 'Name', field: 'name' },
    { headerName: 'Active', field: 'isActive' },
    { headerName: 'CreatedAt', field: 'createdAt' },
    { headerName: 'Action', width: 250, cellRendererFramework: ActionGridCellComponent },
  ];
  categoryModel: Array<ICategory>;
  context: any;
  ngOnInit() {
    this.context = { parent: this };
  }

  onItemEdit(entity: ICategory) {
    this.modalService.open(
      {
        title: `Edit ${entity.name}`,
        activeComponent: CategoryDetailsComponent,
        data: entity,
        componentMode: ComponentMode.Edit
      });
  }
  onItemDelete(data: ICategory) {
    console.log(`${data.name} is deleting`);
  }
  onItemInfo(data: ICategory) {
    console.log(`${data.name} info is showing`);
  }

  onGridReady(params: AgGridEvent) {
    this.loadData();
    this.categoryService.reloadReqired.asObservable().subscribe((r) => { if (r) { this.loadData(); } });
  }
  private loadData(): void {
    this.categoryService.findAll().subscribe(
      r => {
        this.categoryModel = r.data;
      });
  }

}
