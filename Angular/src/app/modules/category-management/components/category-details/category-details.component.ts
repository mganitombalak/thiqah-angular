import { Component, OnInit } from '@angular/core';
import { ICategory } from 'src/app/common/models/ICategory';
import { BaseModalComponent } from 'src/app/common/base/BaseModalComponent';
import { CategoryService } from '../../services/category.service';
import { ModalService } from 'src/app/modules/shared/services/modal.service';
import { Observable } from 'rxjs';
import { IResponse } from 'src/app/common/models/IResponse';
import { JsHelper } from 'src/app/common/helpers/JsHelper';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.css']
})
export class CategoryDetailsComponent extends BaseModalComponent<ICategory> implements OnInit {

  constructor(private categoryService: CategoryService, private modalService: ModalService) {
    super();
  }

  ngOnInit() {
  }

  onUpsertSubmit() {
    let serviceObservable: Observable<IResponse<ICategory>>;
    switch (this.componentMode) {
      case ComponentMode.Insert:
        {
          const data = JsHelper.toFormData(this.model);
          data.set('iconFile', null);
          console.log('create api will be called');
        }
        break;
      case ComponentMode.Edit:
        {
          const data = JsHelper.toFormData(this.model);
          data.set('iconFile', null);
          serviceObservable = this.categoryService.update(data);
          this.categoryService.reloadReqired.emit(true);
        }
        break;
      case ComponentMode.Delete:
        console.log('delete api will be called');
        break;
    }
    serviceObservable.subscribe({
      next: this.onNext.bind(this),
      error: this.onError.bind(this),
      complete: this.onComplete.bind(this)
    });
  }
  private onNext() { }
  private onComplete() { this.modalService.close(); this.categoryService.reloadReqired.emit(true); }
  private onError() { }

}
