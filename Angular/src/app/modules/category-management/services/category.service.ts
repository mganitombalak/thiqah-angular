import { Injectable } from '@angular/core';
import BaseService from 'src/app/common/base/BaseService';
import { ICategory } from 'src/app/common/models/ICategory';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class CategoryService extends BaseService<ICategory> {
  constructor(public httpClient: HttpClient) {
    super(httpClient);
    this.endpoint = 'category';
  }
}
