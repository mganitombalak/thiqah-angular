import { NgModule } from '@angular/core';

import { CategoryManagementRoutingModule } from './category-management-routing.module';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { CategoryService } from './services/category.service';
import { AgGridModule } from 'ag-grid-angular';
import { SharedModule } from '../shared/shared.module';
import { ActionGridCellComponent } from '../shared/components/action-grid-cell/action-grid-cell.component';
import { CategoryDetailsComponent } from './components/category-details/category-details.component';

@NgModule({
  declarations: [CategoryListComponent, CategoryDetailsComponent],
  imports: [
    SharedModule,
    AgGridModule.withComponents([ActionGridCellComponent]),
    CategoryManagementRoutingModule
  ],
  providers: [CategoryService],
  entryComponents: [CategoryDetailsComponent]
})
export class CategoryManagementModule { }
