import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { IAuthRequest } from 'src/app/common/models/IAuthRequest';
import { IAuthResponse } from 'src/app/common/models/IAuthResponse';

@Injectable({ providedIn: 'root' })
export class AuthService {
  onAuthStatusChanged: BehaviorSubject<boolean>;
  onAuthError: BehaviorSubject<boolean>;
  constructor(private httpClient: HttpClient, private router: Router) {
    this.onAuthStatusChanged = new BehaviorSubject(this.isAuthenticated() || false);
    this.onAuthError = new BehaviorSubject(null);
  }

  login(loginInfo: IAuthRequest): void {
    const auth = this.httpClient.post('auth', JSON.stringify(loginInfo)) as Observable<IAuthResponse>;
    auth.pipe(take(1)).subscribe((res) => {
      localStorage.setItem('userData', JSON.stringify(res.data));
      localStorage.setItem('token', res.token);
      this.onAuthStatusChanged.next(true);
      this.router.navigate(['/']);
    }, e => this.onAuthError.next(e));
  }

  logout(): void {
    localStorage.clear();
    this.onAuthStatusChanged.next(false);
    this.router.navigate(['/auth']);
  }

  isAuthenticated = () => !!localStorage.getItem('token');


}