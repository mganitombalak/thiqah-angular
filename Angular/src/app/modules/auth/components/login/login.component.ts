import { Component, OnInit } from '@angular/core';
import { IAuthRequest } from 'src/app/common/models/IAuthRequest';
import { AuthService } from '../../services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: IAuthRequest = { password: '', username: '' } as IAuthRequest;  
  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.login(this.model);
  }

}
