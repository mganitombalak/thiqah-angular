import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ActionGridCellComponent } from './components/action-grid-cell/action-grid-cell.component';
import { ModalComponent } from './components/modal/modal.component';
import { ModalBodyContainerDirective } from 'src/app/common/directives/modal-body-container.directive';



@NgModule({
  declarations: [ActionGridCellComponent, ModalComponent, ModalBodyContainerDirective],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [CommonModule, ActionGridCellComponent, ModalComponent, ModalBodyContainerDirective, FormsModule]
})
export class SharedModule { }
