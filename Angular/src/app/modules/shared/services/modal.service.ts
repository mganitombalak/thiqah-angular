import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { IModelSetupOptions } from 'src/app/common/models/IModalSetupOptions';
import { IModalOptions } from 'src/app/common/models/IModalOption';

@Injectable({ providedIn: 'root' })
export class ModalService {

  modalSetupOptions: IModelSetupOptions = { showFooter: false, showHeader: true };
  modalOptions: IModalOptions;
  private renderer: Renderer2;
  constructor(private rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  setup(setupOptions: IModelSetupOptions): void {
    this.modalSetupOptions.modalComponent = setupOptions.modalComponent;
    this.modalSetupOptions.modalContainer = setupOptions.modalContainer;
  }

  open(options: IModalOptions): void {
    this.modalOptions = options;
    this.modalSetupOptions.modalComponent.onOpening();
    this.renderer.setStyle(this.modalSetupOptions.modalContainer.nativeElement, 'display', 'block');
    setTimeout(() => this.renderer.addClass(this.modalSetupOptions.modalContainer.nativeElement, 'show'), 100);
  }

  close(): void {
    this.modalSetupOptions.modalComponent.onClosing();
    this.renderer.setStyle(this.modalSetupOptions.modalContainer.nativeElement, 'display', 'none');
    setTimeout(() => this.renderer.addClass(this.modalSetupOptions.modalContainer.nativeElement, 'hıde'), 100);
  }

}
