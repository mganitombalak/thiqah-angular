import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ComponentFactoryResolver } from '@angular/core';
import { ModalService } from '../../services/modal.service';
import { ModalBodyContainerDirective } from 'src/app/common/directives/modal-body-container.directive';
import { BaseModalComponent } from 'src/app/common/base/BaseModalComponent';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, AfterViewInit {

  @ViewChild('modalContainer', { static: false }) modalContainer: ElementRef;
  @ViewChild(ModalBodyContainerDirective, { static: false }) modalBodyContainer: ModalBodyContainerDirective;
  constructor(public modalService: ModalService, private componentFactoryResolver: ComponentFactoryResolver) { }
  ngOnInit() { }
  ngAfterViewInit(): void {
    this.modalService.setup({ modalContainer: this.modalContainer, modalComponent: this });
  }

  onOpening(): void {
    if (this.modalBodyContainer) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.modalService.modalOptions.activeComponent);
      const componentRef = this.modalBodyContainer.element.createComponent(componentFactory);
      (componentRef.instance as BaseModalComponent<any>).bind({
        data: this.modalService.modalOptions.data,
        componentMode: this.modalService.modalOptions.componentMode
      });
    }
  }
  onClosing(): void {
    this.modalBodyContainer.element.clear();
  }
}
