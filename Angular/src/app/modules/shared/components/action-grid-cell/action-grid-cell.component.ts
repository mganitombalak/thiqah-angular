import { Component } from '@angular/core';
import { ICellRendererParams, IAfterGuiAttachedParams } from 'ag-grid-community';
import { AgRendererComponent } from 'ag-grid-angular';

@Component({
  selector: 'app-action-grid-cell',
  templateUrl: './action-grid-cell.component.html',
  styleUrls: ['./action-grid-cell.component.css']
})
export class ActionGridCellComponent implements AgRendererComponent {
  private params: any;
  refresh(params: any): boolean {
    return false;
  }

  agInit(params: ICellRendererParams): void {
    this.params = params;
  }

  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {
    this.params = params;
  }

  onEditClicked() {
    this.params.context.parent.onItemEdit(this.params.data);
  }
  onDeleteClicked() {
    this.params.context.parent.onItemDelete(this.params.data);
  }
  onInfoClicked() {
    this.params.context.parent.onItemInfo(this.params.data);
  }
}