import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthGuard } from './modules/auth/guards/auth.guard';


const routes: Routes = [
  { path: '', canActivate: [AuthGuard], component: DashboardComponent },
  {
    path: 'auth',
    loadChildren: './modules/auth/auth.module#AuthModule'
  },
  {
    path: 'category-management',
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: './modules/category-management/category-management.module#CategoryManagementModule'
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
