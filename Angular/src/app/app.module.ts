import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app-root/app.component';
import { HeaderComponent } from './components/header/header.component';
import { DropDownMenuDirective } from './common/directives/dropdown-mnu.directive';
import { AsideComponent } from './components/aside/aside.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MenuHoverDirective } from './common/directives/menu-hover.directive';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { BaseInterceptor } from './common/interceptors/base-interceptor';
import { ArrayCheckPipe } from './common/pipes/array-check.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DropDownMenuDirective,
    MenuHoverDirective,
    AsideComponent,
    DashboardComponent,
    CarouselComponent,
    ArrayCheckPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: BaseInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
