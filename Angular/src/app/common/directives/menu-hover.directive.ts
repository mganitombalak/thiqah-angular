import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({ selector: '[appMenuHover]' })
export class MenuHoverDirective {
    constructor(private element: ElementRef, private render: Renderer2) { }

    @HostListener('mouseenter')
    onMouserEnter(): void {
        this.render.addClass(this.element.nativeElement, 'active');
    }

    @HostListener('mouseleave')
    onMouserLeave(): void {
        this.render.removeClass(this.element.nativeElement, 'active');
    }
}
