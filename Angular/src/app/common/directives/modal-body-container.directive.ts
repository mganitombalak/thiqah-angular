import { ViewContainerRef, Directive } from '@angular/core';

@Directive({ selector: '[appModalBody]' })
export class ModalBodyContainerDirective {
    constructor(public element: ViewContainerRef) { }
}
