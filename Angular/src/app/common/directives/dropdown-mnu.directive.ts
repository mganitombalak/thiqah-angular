import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({ selector: '[appDropDownMenu]' })
export class DropDownMenuDirective {
    constructor(private element: ElementRef, private renderer: Renderer2) {
        // console.log("hey directive is get instantiated.:");
    }

    private subMenuContainer: any;
    @HostListener('mouseenter', ['$event'])
    onMouseEnter(event: Event) {
        this.subMenuContainer = this.element.nativeElement.querySelector('.dropdown-menu');
        this.renderer.addClass(this.subMenuContainer, 'd-block');
        this.renderer.removeClass(this.subMenuContainer, 'd-none');
    }
    @HostListener('mouseleave', ['$event'])
    onMouseLeave(event: Event) {
        this.subMenuContainer = this.element.nativeElement.querySelector('.dropdown-menu');
        this.renderer.addClass(this.subMenuContainer, 'd-none');
        this.renderer.removeClass(this.subMenuContainer, 'd-block');
    }


}

