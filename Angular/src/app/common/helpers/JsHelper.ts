export class JsHelper {
    static toFormData(param: any): FormData {
        if (param) {
            const formData = new FormData();
            Object.keys(param).forEach(field => formData.append(field, param[field]));
            return formData;
        }
        return null;
    }
}
