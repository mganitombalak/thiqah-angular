import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IResponse } from '../models/IResponse';
import { EventEmitter } from '@angular/core';

export default class BaseService<T> {
    reloadReqired: EventEmitter<boolean>;
    protected endpoint: string;
    constructor(public httpClient: HttpClient) {
        this.reloadReqired = new EventEmitter();
    }

    findAll(): Observable<IResponse<T>> {
        return this.httpClient.get(this.endpoint) as Observable<IResponse<T>>;
    }
    deleteById(id: any): Observable<IResponse<T>> {
        return this.httpClient.delete(this.endpoint + `/${id}`) as Observable<IResponse<T>>;
    }

    update(model: T | FormData): Observable<IResponse<T>> {
        return this.httpClient.put(this.endpoint, model) as Observable<IResponse<T>>;
    }
}
