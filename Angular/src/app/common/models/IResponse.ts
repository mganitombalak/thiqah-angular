export interface IResponse<T> {
    totalRecorNumber: number;
    data: Array<T>;
    singleData: T;
    humanReadableMessage: string[];
}
