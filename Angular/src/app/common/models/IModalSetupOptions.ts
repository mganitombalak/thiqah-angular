import { ElementRef } from '@angular/core';
import { ModalComponent } from 'src/app/modules/shared/components/modal/modal.component';

export interface IModelSetupOptions {
    modalContainer?: ElementRef;
    modalComponent?: ModalComponent;
    showHeader?: boolean;
    showFooter?: boolean;
}