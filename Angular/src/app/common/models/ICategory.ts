export interface ICategory {
    id: string;
    name: string;
    icon: string;
    displayOrder: number;
    iconFile: string;
    iconPath: string;
    iconFileContentType: string;
}