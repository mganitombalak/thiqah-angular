import { Type } from '@angular/core';

export interface IModalOptions {
    title?: string;
    componentMode: ComponentMode;
    activeComponent?: Type<any>;
    data?: any;
}
