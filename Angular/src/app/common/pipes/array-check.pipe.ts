import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'arrayCheck' })
export class ArrayCheckPipe implements PipeTransform {

  transform(value: Array<any>): boolean {
    return  Array.isArray(value) && value.length > 0;
  }

}
